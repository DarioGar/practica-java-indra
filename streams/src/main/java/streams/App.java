package streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;



public class App {

	public static void main(String[] args) {
		
		Person p1 = new Person(1,"Jose","Garc�a",24d,"Software Engineer I");
		Person p2 = new Person(2,"Pepe","Fernandez",25d,"Software Engineer II");
		Person p3 = new Person(3,"Juan","Suarez",26d,"Software Engineer III");
		Person p4 = new Person(5,"Dar�o","Suarez",28d,"Software Engineer II");
		Person p5 = new Person(9,"Nadia","Suarez",32d,"Software Engineer III");
		Person p6 = new Person(10,"Lu�s","Garc�a",33d,"Software Engineer I");
		
		List<Project> projects = new ArrayList<Project>();

		projects.add(new Project(1,p1,"Proyecto 1","Trenes","Transporte",100000));
		projects.add(new Project(2,p2,"Proyecto 2","FCAS","Defensa",123456));
		projects.add(new Project(3,p3,"Proyecto 3","Galileo","Radar",125000));
		projects.add(new Project(4,p4,"Proyecto 4","Trenes","Transporte",140500));
		projects.add(new Project(5,p5,"Proyecto 5","FCAS","Defensa",140000));
		projects.add(new Project(6,p6,"Proyecto 6","Galileo","Radar",250000));
		projects.add(new Project(7,p2,"Proyecto 7","Trenes","Transporte",111111));
		projects.add(new Project(8,p3,"Proyecto 8","FCAS","Defensa",222222));
		projects.add(new Project(9,p3,"Proyecto 9","Galileo","Radar",333333));
		projects.add(new Project(10,p5,"Proyecto 10","Trenes","Transporte",90000));

		
		iterateStreamObtainingDistinct(projects);
	}
	
	private static void iterateStream(List<Project> projects) {
		Stream<Project> projectsStream = projects.stream();
		projectsStream.forEach(project -> System.out.println(project));
	}
	
	//Concatenando filtros
	private static void iterateStreamConcatenateFilters(List<Project> projects) {
		Stream<Project> projectsStream = projects.stream();
		projectsStream.filter(project ->project.getAreaOfWork()=="Transporte").filter(project -> project.getId() > 2).forEach(p -> System.out.println(p));;
	}
	
	//Recorriendo stream en combinacion con iterator
	private static void iterateFilterWithIterator(List<Project> projects) {
		Stream<Project> projectsStream = projects.stream();
		
		Iterator<Project> it = projectsStream.filter(project -> project.getCreator().getName().equals("Juan")).iterator();
		
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	// Obtener el máximo de un stream
	private static void iterateStreamObtainingMax(List<Project> projects) {
		Stream<Project> projectsStream = projects.stream();
		
		Comparator<Project> comparator = new Comparator<Project>() {
			public int compare(Project p1,Project p2) {
				return p1.getBudget() - p2.getBudget();
			}
		};
		Optional<Project> projectMax = projectsStream.max(comparator);
		System.out.println(projectMax.isPresent() ? "Presupuesto mayor: " + projectMax.get().getName() + " con: " + projectMax.get().getBudget() : "Algo salió mal");
		
		projectsStream = projects.stream();
		Optional<Project> projectMin = projectsStream.min(comparator);
		System.out.println(projectMin.isPresent() ? "Presupuesto menor: " + projectMin.get().getName() + " con: " + projectMin.get().getBudget()  : "Algo salió mal");
	}
	
	//Utilizando operacion distinct para obtener elementos únicos
	private static void iterateStreamObtainingDistinct(List<Project> projects) {
		Stream<Project> projectStream = projects.stream();
		//Yo que se :D
		projectStream.map(p -> p.getCreator()).distinct().forEach(project -> System.out.println(project));
	}

}
