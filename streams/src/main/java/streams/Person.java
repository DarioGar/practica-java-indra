package streams;

public class Person {

	private Integer id;
	private String name;
	private String surname;
	private Double age;
	private String position;

	public Person(Integer id, String name,String surname, Double age, String position) {
		super();
		this.id = id;
		this.surname = surname;
		this.name = name;
		this.age = age;
		this.position = position;
	}

	
	
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", surname=" + surname + ", age=" + age + ", position="
				+ position + "]";
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Double getAge() {
		return age;
	}

	public void setAge(Double age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String area) {
		this.position = position;
	}

	public Person() {

	}
}
