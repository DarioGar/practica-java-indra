package streams;

public class Project {

	private Integer id;
	private Person creator;
	private String name;
	private String subject;
	private String areaOfWork;
	private Integer budget;
	
	
	
	@Override
	public String toString() {
		return "Project [id=" + id + ", creator=" + creator + ", name=" + name + ", subject=" + subject
				+ ", areaOfWork=" + areaOfWork + ", budget=" + budget + "]";
	}
	
	public Project(Integer id, Person creator, String name, String subject, String areaOfWork, Integer budget) {
		super();
		this.id = id;
		this.creator = creator;
		this.name = name;
		this.subject = subject;
		this.areaOfWork = areaOfWork;
		this.budget = budget;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Person getCreator() {
		return creator;
	}
	public void setCreator(Person creator) {
		this.creator = creator;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAreaOfWork() {
		return areaOfWork;
	}
	public void setAreaOfWork(String areaOfWork) {
		this.areaOfWork = areaOfWork;
	}
	public Integer getBudget() {
		return budget;
	}
	public void setBudget(Integer budget) {
		this.budget = budget;
	}
	


}
