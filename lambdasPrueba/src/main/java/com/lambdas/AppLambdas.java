package com.lambdas;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class AppLambdas {

	public static void main(String[] args) {
		
		//AppLambdas.lambdaBodyAndParameter();
		//AppLambdas.lambdaReturnNumber();
		//AppLambdas.lambdaConsumer();
		//AppLambdas.lambdaSupplier();
		//AppLambdas.lambdaFunction();
		AppLambdas.lambdaPredicate();

	}
	
	public static void lambdaBodyAndParameter() {	
		Integer number = 5;
		
		lambdaOneParameterAndBody addfive = new lambdaOneParameterAndBody() {

			@Override
			public void add(Integer number) {
				System.out.println("Always add five... +" + number);				
			}
			
		};
		
		lambdaOneParameterAndBody callLmbdExp = newAddFive -> System.out.println("More five: +" + newAddFive);
		callLmbdExp.add(number);
		addfive.add(number);
	}
	
	public static void lambdaReturnNumber() {
		Integer number = 8;
		
		lambdaReturnValues lmbdReturn = new lambdaReturnValues() {

			@Override
			public Integer moreNumbers(Integer number) {
				System.out.println("My favourite number is eight..." + number);
				return number;
			}
			
		};
		
		lambdaReturnValues callLmbdExp = incomingAdd -> { System.out.println("My favourite number: " + number);
		return incomingAdd; 
		};
		
		System.out.println(callLmbdExp.moreNumbers(number));
		System.out.println(lmbdReturn.moreNumbers(number));
	}
	
	public static void lambdaConsumer() {
		Consumer<Integer> consumerNumber = favNumber -> System.out.println("My new favourite number is: " + favNumber);
		consumerNumber.accept(9);
	}
	
	public static void lambdaSupplier() {
		Supplier<Integer> supplierNumber = () -> 10;
		System.out.println("Another favourite number: " + supplierNumber.get());
	}
	
	public static void lambdaFunction() {
		Function<Integer, Integer> numFunction = favNumber -> favNumber+1;
		Integer favNumberFunction = numFunction.apply(10);
		System.out.println("My favourite number +1: " + favNumberFunction);
	}
	
	public static void lambdaPredicate() {
		Predicate<Integer> numberDif = favNumber -> favNumber > 150;
		boolean firstNumber = numberDif.test(225);
		boolean secondNumber = numberDif.test(125);
		
		System.out.println(firstNumber);
		System.out.println(secondNumber);
	}

}
