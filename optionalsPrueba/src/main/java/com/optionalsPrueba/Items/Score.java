package com.optionalsPrueba.Items;

import java.util.Optional;

public class Score {
	
	private Optional<Object> winner;
	private Optional<Referee> referee;
	
	public Score() {
		winner = Optional.empty();
		referee = Optional.empty();
	}

	public Score(String winner, Optional<Referee> referee) {
		this.winner = Optional.of(winner);
		this.referee = referee;
	}

	public Optional<Referee> getReferee() {
		return referee;
	}

	public void setReferee(Optional<Referee> referee) {
		this.referee = referee;
	}

	public Optional<Object> getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = Optional.of(winner);
	}

	@Override
	public String toString() {
		return "Score [winner=" + winner + ", referee=" + referee + "]";
	}
	
}
