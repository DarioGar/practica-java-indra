package com.optionalsPrueba.Items;

public class Referee {
	
	private Integer id;
	private String name;
	
	public Referee(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Referee [id=" + id + ", name=" + name + "]";
	}
	
	
	
	

}
