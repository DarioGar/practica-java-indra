package com.optionalsPrueba.Items;

import java.util.Optional;

public class Match {
	
	private Integer id;
	private String team1;
	private String team2;
	private Optional<Score> winner;
	
	public Match(Integer id, String team1, String team2, Optional<Score> winner) {
		super();
		this.id = id;
		this.team1 = team1;
		this.team2 = team2;
		this.winner = winner;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTeam1() {
		return team1;
	}
	public void setTeam1(String team1) {
		this.team1 = team1;
	}
	public String getTeam2() {
		return team2;
	}
	public void setTeam2(String team2) {
		this.team2 = team2;
	}
	public Optional<Score> getWinner() {
		return winner;
	}
	public void setWinner(Optional<Score> winner) {
		this.winner = winner;
	}
	@Override
	public String toString() {
		return "Match [id=" + id + ", team1=" + team1 + ", team2=" + team2 + ", winner=" + winner + "]";
	}
	
	
	

}
