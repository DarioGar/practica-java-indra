package com.optionalsPrueba.Busqueda;

import java.util.Optional;

import com.optionalsPrueba.Items.Match;
import com.optionalsPrueba.Items.Referee;
import com.optionalsPrueba.Items.Score;

public class App {

	public static void main(String[] args) {
		ResultSearch so = new ResultSearch();
		
		//System.out.println(so.searchOptional(3).get());
		
		//App.BusquedaAnidada(3);
		//App.BusquedaAnidada(2);
		//App.BusquedaAnidada(1);
		
		App.BusquedaLambda(2);
		App.BusquedaLambda(1);

	}
	
	private static void BusquedaAnidada(Integer id) {
		
		ResultSearch so = new ResultSearch();
		
		Optional<Match> so2 = so.searchOptional(id);
		if(so2.isPresent()) {
			Optional<Score> so3 = so2.get().getWinner();
			if(so3.isPresent()) {
				Optional<Referee> so4 = so3.get().getReferee();
				if(so4.isPresent()) {
					System.out.println("El arbitro es: " + so4);
				}else {
					System.out.println("No hay arbitro");
				}
			}
		}
	}
	
	private static void BusquedaLambda(Integer id) {
		ResultSearch so = new ResultSearch();
		
		Optional<Match> so2 = so.searchOptional(id);
		so2.ifPresent(blambda -> System.out.println(blambda.getTeam1()));
	}

}
