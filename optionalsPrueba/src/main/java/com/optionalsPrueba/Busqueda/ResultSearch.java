package com.optionalsPrueba.Busqueda;

import java.util.Optional;

import com.optionalsPrueba.Items.Match;
import com.optionalsPrueba.Items.Referee;
import com.optionalsPrueba.Items.Score;

public class ResultSearch {
	
	public Optional<Match> searchOptional(Integer id) {
		
		switch(id) {
		
		case 1: return Optional.of(new Match(1, "A", "B", Optional.empty()));
		case 2: return Optional.of(new Match(2, "C", "D", Optional.empty()));
		case 3: return Optional.of(new Match(3, "E", "F", Optional.of(new Score("F", Optional.of(new Referee(1, "Pepe Perez"))))));
		default: return Optional.empty();
		
		}
		
	}

}
