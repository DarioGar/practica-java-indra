package times;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.time.format.DateTimeFormatter;


public class App {
	
	public static String separator = "\n==================================================\n";
	
	public static void main(String[] args) {
		javaLocalDate();
		System.out.println(separator);
		javaLocalTime();
		System.out.println(separator);
		javaDateTimeFormatter();
		System.out.println(separator);
		javaLocalDateTime();
	}
	
	private static void javaLocalDate() {
		LocalDate date = LocalDate.now();
		System.out.println("La fecha es: " + date +" y el d�a del a�o es el: "+ date.getDayOfYear());
	}
	
	private static void javaLocalTime() {
		LocalTime lt1 = LocalTime.now();
		System.out.println("La hora es las: " + lt1.getHour() + " y los nanosegundos son: " + lt1.getNano());
	}
	
	private static void javaDateTimeFormatter() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate ldt = LocalDate.parse("13/09/1995", dtf);
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("dd/MMMM/yyyy");
		String fecha = ldt.format(dtf2);
		System.out.println(fecha);
	}
	
	private static void javaLocalDateTime() {
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);


		System.out.println("D�a del mes: " + ldt.getDayOfMonth());
		System.out.println("D�a de la semana: " + ldt.getDayOfWeek());
		System.out.println("D�a del a�o: " + ldt.getDayOfYear());
		System.out.println("N�mero del mes: " + ldt.getMonthValue());
		System.out.println("Nombre del mes: " + ldt.getMonth());
		System.out.println("A�o: " + ldt.getYear());
	}
}
